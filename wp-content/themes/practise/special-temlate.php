<?php

//Template Name: Special layout

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); ?>

        <article class='post page'>
            <h2><?php the_title(); ?></h2>
            <div class="info-box">
                <h4>disclaimer title</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>

			<?php the_content(); ?>
        </article>

	<?php }
} else {
	echo '<p>No content</p>';
}

get_footer();

?>
