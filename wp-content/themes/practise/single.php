<?php

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();

		get_template_part('content', get_post_format());?>
		<div class="about-author">
			<div class="about-author-image">
				<?php echo get_avatar(get_the_author_meta('ID'), 512); ?>
				<p><?php echo get_the_author_meta('nickname'); ?></p>
			</div>

			<?php $otherAuthorPosts = new WP_Query([
				'author' => get_the_author_meta('ID'),
				'posts_per_page'=>3,
				'post__not_in'=>[get_the_ID()],
			]); ?>

			<div class="about-author-text">
				<h3>about-author</h3>
				<?php echo wpautop(get_the_author_meta('description')); ?>
				<?php if($otherAuthorPosts->have_posts()) : ?>
					<div class="other-posts-by">
						<h4>other posts by <?php echo get_the_author_meta('nickcname') ?></h4>
						<ul>
							<?php while($otherAuthorPosts->have_posts()) {
								$otherAuthorPosts->the_post(); ?>
								<li><a href="<?= the_permalink() ?>"><?= the_title() ?></a></li>
							<?php } ?>
						</ul>
					</div>
				<?php endif; wp_reset_postdata(); ?>

				<?php if(count_user_posts(get_the_author_meta('ID')) > 3) : ?>
					<a class="btn" href="<?= get_author_posts_url(get_the_author_meta('ID')) ?>">
						View all posts by <?= get_the_author_meta('nickname') ?>
					</a>
				<?php endif ?>
			</div>
		</div>
	<?php }
} else {
	echo '<p>No content</p>';
}

get_footer();

?>
