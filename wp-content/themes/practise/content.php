<article class='post <?php if ( has_post_thumbnail() && !is_single() ) : ?>has-thumbnail<?php endif; ?>'>

	<?php if(!is_single()): ?>
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'small-thumbnail' ); ?></a>
		</div>
	<?php endif;?>

    <h2><a <?php echo !is_single() ? "href=\"".get_permalink()."\"" : '' ?>><?php the_title(); ?></a></h2>

    <p class="post-info">
		<?php the_time( 'jS F Y H:i T' ); ?>
        | by
        <a href="<?php echo get_author_posts_url( get_the_author_meta( 'id' ) ); ?>">
			<?php the_author(); ?></a>
        | posted in
		<?php
		$categories = get_the_category();
		$separator  = ', ';
		$output     = '';

		if ( $categories ) {
			/** @var WP_Term $category */
			foreach ( $categories as $category ) {
				$output .= "<a href=\"" . get_category_link( $category->term_id ) . "\">" . $category->cat_name . "</a>" . $separator;
			}
			echo mb_strcut( $output, 0, - 2 );
		}
		?>
    </p>

	<?php if ( is_search() || is_archive() ) { ?>
        <p>
			<?php echo get_the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>">Read more &raquo;</a>
        </p>
	<?php } else {
		if(is_single() && has_post_thumbnail()){
			the_post_thumbnail( 'banner-image' );
		}
		if ( $post->post_excerpt || !is_single()) { ?>
			<p>
				<?php echo get_the_excerpt(); ?>
				<a href="<?php the_permalink(); ?>">Read more &raquo;</a>
			</p>
		<?php } else {
			the_content();
		}
	} ?>
</article>