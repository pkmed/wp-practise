<?php

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); ?>

        <article class='post page'>
			<?php the_content(); ?>
		</article>

	<?php }
} else {
	echo '<p>No content</p>';
}

// opinion posts loop
$opinionPosts = new WP_Query('cat=4&posts_per_page=2');
if ( $opinionPosts->have_posts() ) {
	while ( $opinionPosts->have_posts() ) {
		$opinionPosts->the_post(); ?>

		<h2><?php the_title(); ?></h2>

	<?php }
} else {
	echo '<p>No content</p>';
}
wp_reset_postdata();

// news posts loop
$newsPosts = new WP_Query('cat=5&posts_per_page=2');
if ( $newsPosts->have_posts() ) {
	while ( $newsPosts->have_posts() ) {
		$newsPosts->the_post(); ?>

		<h2><?php the_title(); ?></h2>

	<?php }
} else {
	echo '<p>No content</p>';
}
wp_reset_postdata();

get_footer();

?>
