<footer class="site-footer">
	<?php if(get_theme_mod('p-footer-callout-display')=="Yes") :?>
		<div class="footer-callout">
			<div class="footer-callout-image">
				<img src="<?php echo wp_get_attachment_url(get_theme_mod('p-footer-callout-image')); ?>">
			</div>
			<div class="footer-callout-text">
				<h2><a href="<?php echo get_permalink(get_theme_mod('p-footer-callout-link')); ?>">
					<?php echo get_theme_mod('p-footer-callout-headline'); ?>
				</a></h2>
				<p><?php echo wpautop(get_theme_mod('p-footer-callout-paragraph')); ?></p>
			</div>
		</div>
	<?php endif;?>

	<div class="nav-and-search">
		<nav class="site-nav">
			<?php wp_nav_menu( [ 'theme_location' => 'footer' ] ); ?>
		</nav>
		<?php dynamic_sidebar('footer_widget_1'); ?>
	</div>
	<p><?php echo bloginfo( 'name' ) . ' - &copy; ' . date( 'Y' ); ?></p>
</footer>

</div>

<?php wp_footer(); ?>
</body>
</html>
