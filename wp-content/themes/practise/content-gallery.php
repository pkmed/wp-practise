<article class="post post-gallery">
	<h2><a href="<?php the_permalink(); ?>"><?= the_title(); ?></a></h2>
	<?php the_content(); ?>
</article>