<?php

function practise_resources() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );
}

add_action( 'wp_enqueue_scripts', 'practise_resources' );

// Get top ancestor
function get_top_ancestor_id() {

	global $post;

	/** @var WP_Post $post */
	if ( $post->post_parent ) {
		$ancestors = get_post_ancestors( $post->ID );

		return $ancestors[0];
	}

	return $post->ID;
}

function has_children() {
	global $post;

	$pages = get_pages( 'child_of=' . $post->ID );

	return count( $pages );
}

//Customize excerpt word count length
function custom_excerpt_length() {
	return 25;
}

add_filter( 'excerpt_length', 'custom_excerpt_length' );

//theme setup
function practise_setup() {
	//Nav menus
	register_nav_menus( [
		'primary' => __( 'Primary Menu' ),
		'footer'  => __( 'Footer Menu' ),
	] );

	//Featured image support
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'small-thumbnail', 180, 120, true );
	add_image_size( 'banner-image', 920, 120, [ 'left', 'center' ] );

	//Add post format support
	add_theme_support('post-formats', ['aside', 'gallery', 'link']);
	
	//widgets
	register_sidebar( array(
		'name'          => 'Home right sidebar',
		'id'            => 'home_right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded sidebar-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => 'Footer widget',
		'id'            => 'footer_widget_1',
		'before_widget' => '<div class="footer-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded widget-title">',
		'after_title'   => '</h2>',
	) );
}

add_action( 'after_setup_theme', 'practise_setup' );

//customize appearance 
function practise_customize_register( WP_Customize_Manager $wp_customize ){
	$wp_customize->add_setting('p_link_color', [
		'default'=> '#006ec3',
		'transport'=> 'refresh',
	]);

	$wp_customize->add_setting('p_btn_color', [
		'default'=> '#006ec3',
		'transport'=> 'refresh',
	]);

	$wp_customize->add_setting('p_btn_hover_color', [
		'default'=> '#006ec3',
		'transport'=> 'refresh',
	]);

	$wp_customize->add_section('p_standard_colors',[
		'title'=>__('Standard colors', 'practise'),
		'priority' => 30,
	]);

	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize,'p_link_color_control', [
		'label' => __('Link color', 'practise'),
		'section' => 'p_standard_colors',
		'settings' => 'p_link_color',
	]) );

	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize,'p_btn_color_control', [
		'label' => __('Button color', 'practise'),
		'section' => 'p_standard_colors',
		'settings' => 'p_btn_color',
	]) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize,'p_btn_hover_color_control', [
		'label' => __('Button hover color', 'practise'),
		'section' => 'p_standard_colors',
		'settings' => 'p_btn_hover_color',
	]) );
}

add_action('customize_register', 'practise_customize_register');

//output customize css
function practise_customize_css(){?>
	<style type="text/css">
		a:link,
		a:visited{
			color: <?php echo get_theme_mod('p_link_color'); ?>;
		}

		.site-header nav ul li.current-menu-item a:link,
		.site-header nav ul li.current-menu-item a:visited,
		.site-header nav ul li.current-page-ancestor a:link,
		.site-header nav ul li.current-page-ancestor a:visited{
			background-color: <?php echo get_theme_mod('p_link_color'); ?>;
		}

		#searchsubmit{
			background-color: <?php echo get_theme_mod('p_btn_color'); ?>;
		}

		#searchsubmit:hover{
			background-color: <?php echo get_theme_mod('p_btn_hover_color'); ?>;
		}
	</style>
<?php }

add_action('wp_head', 'practise_customize_css');

//footer callout section
function practise_footer_callout(WP_Customize_Manager $wp_customize){
	$wp_customize->add_section('p-footer-callout-section',[
		'title'=> 'Footer callout',
	]);

	$wp_customize->add_setting('p-footer-callout-display', [
		'default' => 'No'
	]);

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'p-footer-callout-display-control',[
		'label' => 'Display this section?',
		'section' => 'p-footer-callout-section',
		'settings' => 'p-footer-callout-display',
		'type'=>'select',
		'choices'=>[
			'No'=>'No',
			'Yes'=>'Yes',
		]
	]));

	$wp_customize->add_setting('p-footer-callout-headline', [
		'default' => 'example headline text!'
	]);

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'p-footer-callout-headline-control',[
		'label' => 'Headline',
		'section' => 'p-footer-callout-section',
		'settings' => 'p-footer-callout-headline',
	]));

	$wp_customize->add_setting('p-footer-callout-paragraph', [
		'default' => 'example paragraph text!'
	]);

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'p-footer-callout-paragraph-control',[
		'label' => 'Paragraph',
		'section' => 'p-footer-callout-section',
		'settings' => 'p-footer-callout-paragraph',
		'type' => 'textarea',
	]));

	$wp_customize->add_setting('p-footer-callout-link');

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'p-footer-callout-link-control',[
		'label' => 'Link',
		'section' => 'p-footer-callout-section',
		'settings' => 'p-footer-callout-link',
		'type' => 'dropdown-pages',
	]));
	
	$wp_customize->add_setting('p-footer-callout-image');

	$wp_customize->add_control(new WP_Customize_Cropped_Image_Control($wp_customize, 'p-footer-callout-image-control',[
		'label' => 'Image',
		'section' => 'p-footer-callout-section',
		'settings' => 'p-footer-callout-image',
		'width'=>750,
		'height'=>500,
	]));
}

add_action('customize_register', 'practise_footer_callout');