<?php

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); ?>

        <article class='post page'>
			<?php /** @var WP_Post $post */
			if ( has_children() || $post->post_parent > 0 ) { ?>
                <nav class="site-nav children-links clearfix">

				<span class="parent-link">
					<a href="<?php echo get_the_permalink( get_top_ancestor_id() ); ?>">
						<?php echo get_the_title( get_top_ancestor_id() ); ?>
					</a>
				</span>

                    <ul>
						<?php
						$args = [
							'child_of' => get_top_ancestor_id(),
							'title_li' => '',
						];

						wp_list_pages( $args );
						?>
                    </ul>
                </nav>
			<?php } ?>

            <h2><?php the_title(); ?></h2>
			<?php the_content(); ?>
        </article>

	<?php }
} else {
	echo '<p>No content</p>';
} ?>

<h1>blog posts about us</h1>
<?php 
$curPage = get_query_var('paged');

$aboutPosts = new WP_Query([
    'category_name'=>'random',
    'posts_per_page'=>3,
    'paged' => $curPage,
]);

if($aboutPosts->have_posts()) :
    while($aboutPosts->have_posts()) : 
        $aboutPosts->the_post(); ?>
        <li><a href="<?= the_permalink() ?>"><?= the_title() ?></a></li>
        <?php endwhile;
        echo paginate_links([
            'total'=> $aboutPosts->max_num_pages,
        ]);
endif;

?>

<?php

get_footer();

?>
