<?php

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); ?>

        <!-- column-container -->
        <div class="column-container clearfix">
            <div class="title-column">
                <h2><?php the_title(); ?></h2>
            </div>

            <div class="text-column">
				<?php the_content(); ?>
            </div>
        </div>
        <!-- /column-container -->

	<?php }
} else {
	echo '<p>No content</p>';
}

get_footer();

?>
